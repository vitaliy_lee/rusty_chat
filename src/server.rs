use mio::*;
use std::collections::HashMap;
use mio::tcp::TcpListener;
use client::WebSocketClient;

const SERVER: Token = Token(0);

pub struct WebSocketServer {
    socket: TcpListener,
    clients: HashMap<Token, WebSocketClient>,
    token_counter: usize
}

impl WebSocketServer {

    pub fn new(host_port: &str) -> WebSocketServer {
        let addr = host_port.parse().unwrap();

        // Setup the server socket
        let socket = TcpListener::bind(&addr).unwrap();

        WebSocketServer {
            token_counter: 1,
            clients: HashMap::new(),
            socket: socket
        }
    }

	pub fn run(&mut self) {
		// Create a poll instance
		let poll = Poll::new().unwrap();

		// Start listening for incoming connections
		poll.register(&self.socket, SERVER, Ready::readable(),
		              PollOpt::edge()).unwrap();

		// Create storage for events
		let mut events = Events::with_capacity(1024);

		loop {
		    poll.poll(&mut events, None).unwrap();

		    for event in events.iter() {
                if event.readiness().is_readable() {
                    match event.token() {
                        SERVER => {
                            println!("Server handling request");

                            let stream = match self.socket.accept() {
                                Err(e) => {
                                    println!("Failed to connect: {}", e);
                                    continue;
                                },

                                Ok((stream, _)) => stream
                            };

                            let new_token = Token(self.token_counter);
                            self.clients.insert(new_token, WebSocketClient::new(stream));
                            self.token_counter += 1;

                            poll.register(&self.clients[&new_token].socket, new_token, Ready::readable(), PollOpt::edge() | PollOpt::oneshot()).unwrap();
                        },

                        token => {
                            let mut client = self.clients.get_mut(&token).unwrap();
                            client.read();
                            poll.reregister(&client.socket, token, client.interest, PollOpt::edge() | PollOpt::oneshot()).unwrap();
                        }
                    }
                }

                if event.readiness().is_writable() {
                    let token = event.token();
                    let mut client = self.clients.get_mut(&token).unwrap();
                    client.write();
                    poll.reregister(&client.socket, token, client.interest,
                              PollOpt::edge() | PollOpt::oneshot()).unwrap();
                }
		    }
		}
	}
}

