extern crate mio;
extern crate http_muncher;
extern crate sha1;
extern crate rustc_serialize;

mod server;
mod client;
mod http;

const HOST_PORT: &'static str = "127.0.0.1:13265";

fn main() {
    let mut srv = server::WebSocketServer::new(HOST_PORT);
	srv.run()
}
