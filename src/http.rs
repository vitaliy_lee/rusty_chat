use std::collections::HashMap;
use std;
use http_muncher::{Parser, ParserHandler};
use std::rc::Rc;
use std::cell::RefCell;

pub struct HttpParser {
    pub current_key: Option<String>,
    pub headers: Rc<RefCell<HashMap<String, String>>>
}

impl ParserHandler for HttpParser {

	fn on_header_field(&mut self, _: &mut Parser, header: &[u8]) -> bool {
        self.current_key = Some(std::str::from_utf8(header).unwrap().to_string());
        true
    }

	fn on_header_value(&mut self, _: &mut Parser, header: &[u8]) -> bool {
        self.headers.borrow_mut()
            .insert(self.current_key.clone().unwrap(),
                    std::str::from_utf8(header).unwrap().to_string());
        true
    }

    fn on_headers_complete(&mut self, _: &mut Parser) -> bool {
        false
    }
}

