use std::io::Read;
use std::io::Write;
use rustc_serialize::base64::{ToBase64, STANDARD};
use mio::*;
use std::collections::HashMap;
use mio::tcp::TcpStream;
use http::HttpParser;
use std::cell::RefCell;
use std::rc::Rc;
use std::fmt;
use http_muncher::Parser;
use sha1;

const CLIENT_BUF_SIZE: usize = 2048;

#[derive(PartialEq)]
enum ClientState {
    AwaitingHandshake,
    HandshakeResponse,
    Connected
}

pub struct WebSocketClient {
    pub socket: TcpStream,
    pub interest: Ready,
    http_parser: HttpParser,
    headers: Rc<RefCell<HashMap<String, String>>>,
    state: ClientState
}

impl WebSocketClient {

	pub fn new(socket: TcpStream) -> WebSocketClient {
	 	let headers = Rc::new(RefCell::new(HashMap::new()));

        WebSocketClient {
            socket: socket,
            headers: headers.clone(),
            http_parser: HttpParser{current_key: None, headers: headers.clone()},
            interest: Ready::readable(),
            state: ClientState::AwaitingHandshake
        }
    }

    pub fn read(&mut self) {
        loop {
			let mut buf = [0; CLIENT_BUF_SIZE];

            match self.socket.read(&mut buf) {
                Err(e) => {
                    println!("Failed to read data: {:?}", e);
                    return
                },

                Ok(len) => {
                	println!("Read {} bytes", len);

                	let data: &[u8] = &buf[0..len];
                	let mut parser = Parser::request();
                    parser.parse(&mut self.http_parser, data);

                    if parser.is_upgrade() {
                        // Меняем текущее состояние на HandshakeResponse
					    self.state = ClientState::HandshakeResponse;

					    // Меняем набор интересующих нас событий на Writable
					    // (т.е. доступность сокета для записи):
					    self.interest.remove(Ready::readable());
					    self.interest.insert(Ready::writable());

					    break;
                    }
                }
            }
        }
    }

    pub fn write(&mut self) {
        // Заимствуем хеш-таблицу заголовков из контейнера Rc<RefCell<...>>:
        let headers = self.headers.borrow();

        // Находим интересующий нас заголовок и генерируем ответный ключ используя его значение:
        let response_key = gen_key(&headers.get("Sec-WebSocket-Key").unwrap());

        // Мы используем специальную функцию для форматирования строки.
        // Ее аналоги можно найти во многих других языках (printf в Си, format в Python, и т.д.),
        // но в Rust есть интересное отличие - за счет макросов форматирование происходит во время
        // компиляции, и в момент выполнения выполняется только уже оптимизированная "сборка" строки
        // из кусочков. Мы обсудим использование макросов в одной из следующих частей этой статьи.
        let response = fmt::format(format_args!("HTTP/1.1 101 Switching Protocols\r\n\
                                                 Connection: Upgrade\r\n\
                                                 Sec-WebSocket-Accept: {}\r\n\
                                                 Upgrade: websocket\r\n\r\n", response_key));

        // Запишем ответ в сокет:
        self.socket.write(response.as_bytes()).unwrap();

        // Снова изменим состояние клиента:
        self.state = ClientState::Connected;

        // И снова поменяем набор интересующих нас событий на `readable()` (на чтение):
        self.interest.remove(Ready::writable());
        self.interest.insert(Ready::readable());
    }
}

fn gen_key(key: &String) -> String {
    let mut m = sha1::Sha1::new();

    m.update(key.as_bytes());
    m.update("258EAFA5-E914-47DA-95CA-C5AB0DC85B11".as_bytes());

    let data = m.digest().bytes();

    return data.to_base64(STANDARD);
}